# Submarine.jsの使い方

* [デザインコンセプト](./USAGE.md#デザインコンセプト)
* [インストール](./USAGE.md#インストール)
* [チュートリアル](./USAGE.md#チュートリアル)
  - [ディレクトリ構成](./USAGE.md#ディレクトリ構成)
  - [構成ファイル submarine.js](./USAGE.md#構成ファイル-submarinejs)
    * [collect](./USAGE.md#collect)
    * [copy](./USAGE.md#copy)
    * [provision](./USAGE.md#provision)
    * [query](./USAGE.md#query)
    * [test](./USAGE.md#test)
  - [実行結果](./USAGE.md#実行結果)
* [APIリファレンス](./USAGE.md#apiリファレンス)

## デザインコンセプト

Submarine.jsはサーバのライフサイクルを、以下のステージに分けて考えています

* 新規作成(collect)
  * 新しいマシンをセットアップしてからOSを起動するまで
  * ラックに物理サーバをマウントしたり、AWS上に仮想マシンを立てたりすること
  * このステージでのSubmarine.jsの仕事は、起動したOSを検知するところからはじまります
* ファイルコピーと初期構築(copy & provision)
  * OS起動から、ミドルウェアのインストールやセットアップまで
  * NginxやMySQLのインストールなどの作業があります
  * このステージでSubmarine.jsはサーバに必要なファイルをコピーしたり、構築スクリプトを実行する役割を担います
* 状態確認とテスト(query & test)
  * セットアップしたサーバの状態をテストするステージです
  * このステージでSubmarine.jsはサーバの値を取得するShellScriptを実行し、実行結果の値を評価します

## インストール

インストール方法は[こちら](../README.md#インストール方法)


## チュートリアル

### ディレクトリ構成

Submarine.jsでは、以下のようなディレクトリ構成を推奨しています

```
+ work
  + target(グループ名、カレントディレクトリ)
    + submarine.js(構成ファイル)
    + files
      + tmp
        + a.txt
```

* `work` : 作業ディレクトリ
* `target` : 管理対象サーバのグループ名(例. webserver, dbserverなど。submarineコマンド実行時には、ここをカレントディレクトリにしなければならない)
* `submarine.js` : 構成情報を記述するファイル
* `files` : 管理対象サーバにコピーするファイル群

### 構成ファイル submarine.js

submarine.jsの中身は、たとえばこんな感じ

```submarine.js

const collect=[{
  type: 'gen/bash',
  cmd: 'echo {a..c}',
}, {
  type: 'fil/ping',
}];


const copy={
  opts: ['-l submarine'],
  files: [{
    src: './tmp/a.txt',
    dst: '/tmp/a.txt',
  }],
};

const provision={
  gen: 'submarine-test-1',
  opts: ['-l submarine'],
  cmds: [{
    name: 'world',
    cmd: 'echo hello world',
  }, {
    name: 'submarine',
    cmd: 'echo hello submarine',
  }]
};


const query={
  opts: ['-l submarine'],
  query: {
    hostname: 'hostname -s',

    a: String.raw`
      cat /tmp/a.txt
    `,
  },
}

const test={
  func: (host)=>{
    return {
      hostname: host.hostname === 'target001',

      a: host.a === 'a',
    };
  },
};


module.exports={
  collect: collect,
  copy: copy,
  provision: provision,
  query: query,
  test: test,
};
```

* `collect` : 管理対象サーバのIPアドレス、もしくはホスト名を動的に生成する定義
* `copy` : files以下のローカルファイルを、管理対象サーバにコピーするための情報
* `provision` : 管理対象サーバで1回だけ実行される、構築スクリプトの定義
* `query` : テストのため、管理対象サーバから値を取得するスクリプトの定義
* `test` : queryで取得した値をテストする関数の定義

`module.exports` は、上記の定数をSubmarine.jsからロードできるように公開する、Node.jsの書き方です

submarine.jsファイルのあるディレクトリでsubmarineコマンドを実行すると、上記の定義ファイルが読み込まれ、実行されます


ひとつひとつ定数の中身を見ていきましょう

#### collect

```
const collect=[{
  type: 'gen/bash',
  cmd: 'echo server-{a..c}',
}, {
  type: 'fil/ping',
}];
```

`gen/bash` というのはbashジェネレータ、つまりSubmarine.jsにデフォルトで実装されている、IPアドレスやホスト名のリストを生成する機能のことです。同じように `fil/ping` はpingフィルタといって、生成されたリストからpingに応答したものだけを絞り込むフィルタ機能のことです

ですから、この定義は「bashで `echo server-{a..c}` というコマンドを実行し、その結果、つまりserver-aとserver-bとserver-cそれぞれ(スペースや改行で分割される)、に対してpingを打って、応答のあるホストのみ管理対象サーバとする」ということを意味しています


#### copy 

```
const copy={
  opts: ['-l submarine'],
  files: [{
    src: './tmp/a.txt',
    dst: '/tmp/a.txt',
  }],
};
```

copyは、管理対象サーバに対して、ローカルからファイルをコピーする処理を定義します

コピー元である `src` ファイルのパスは `work/target/files` からの相対パスになります。コピー先にあたる `dst` は絶対パスで記述する必要があります

`opts` はsshコマンドのオプションで、ここではログインユーザにsubmarineを指定しています(以降の定義でも同様です)

#### provisoin

```
const provision={
  gen: 'submarine-test-1',
  opts: ['-l submarine'],
  cmds: [{
    name: 'world',
    cmd: 'echo hello world',
  }, {
    name: 'submarine',
    cmd: 'echo hello submarine',
  }]
};
```


`cmds` は、サーバ構築時に1度だけ実行されるコマンドのリストです。`cmd` に書かれているコマンドを実行すると、Submarine.jsはサーバ内の特定のパス(デフォルトでは/var/lib/submarine)に `name` で定義された名前でロックファイルを生成します。2回目以降はこのロックファイルが存在する場合はコマンドをスキップします

サーバの構成管理の世界でよくある冪等性を、ロックファイルで実現しているのです

また `gen` というキーがありますが、これはサーバの「世代(generation)」を定義するものです。`name` 同様、これも初回構築時にサーバ内にロックファイルを作成し、次回以降は世代の一致しないサーバに対してはprovision全体をスキップするような動作になります

この世代管理の機能により、例えばグループ内の一部のサーバだけ、OSやミドルウェアをバージョンアップするためにOSをインストールしなおした場合でも、既存のサーバにはprovisionの処理が実行されずに保護され、新しいサーバのみ構築スクリプトが実行されることになります


#### query

```
const query={
  opts: ['-l submarine'],
  query: {
    hostname: 'hostname -s',

    a: String.raw`
      cat /tmp/a.txt
    `,
  },
}
```

`query` は `名前 : コマンド` のkey-valueペアです。provisionと違い、queryで定義したスクリプトはsubmarineコマンド実行のたびに実行されます。このためquery内では、サーバに変更を加えるようなスクリプトは定義せず、次に説明する `test` でテストしたい値を取得するスクリプトを記述します

この例では、ホスト名の取得と、`/tmp/a.txt` の中身をcatする処理が書かれています(copyでコピーしたファイルの中身を確認しています)

#### test

```
const test={
  func: (host)=>{
    return {
      hostname: host.hostname === 'target001',

      a: host.a === 'a',
    };
  },
};
```

`test` はqueryで取得した値をテストします。provisionが完了したサーバが、正しく構築されているかの確認のために使います

`func` で定義された関数に、queryで取得した結果(ここでは `host` という名前で受け取っている) が引数として渡されるので、queryで定義したキー名で値にアクセスできます


### 実行結果

実行結果は次のようになります

```
{
  "stat": "complete",
  "msg": null,
  "hosts": [
    "server-a"
  ],
  "files": {
    "target": [
      {
        "src": "/work/target/files/./tmp/a.txt",
        "dst": "/tmp/a.txt",
        "stdout": "",
        "stderr": "",
        "stat": "done"
      },
    ]
  },
  "results": {
    "target": [
      {
        "stat": "current",
        "stdout": "submarine-test-1",
        "stderr": ""
      },
      {
        "stat": "done",
        "stdout": "hello world",
        "stderr": ""
      },
      {
        "stat": "done",
        "stdout": "hello submarine",
        "stderr": ""
      }
    ]
  },
  "states": {
    "target": {
      "hostname": "server-a",
      "a": "a",
    }
  },
  "scores": {
    "target": {
      "hostname": true,
      "a": true,
    }
  }
}
```

* `stat` : submarine.jsで定義した処理が、最後まで実行されたか(complete: 実行された、error: 途中でエラー終了した)
* `msg` : statの理由など
* `hosts` : collectで生成したサーバリスト
* `files` : copyでコピーした結果
  - stat: コピー成功したか(done: 成功、error: 失敗)
  - src : コピー元ファイルの絶対パス
  - dst : コピー先ファイル
  - stdout : コピー(rsync)コマンドの標準出力
  - stderr : コピー(rsync)コマンドのエラー出力
* `results` : provisionで定義した世代やコマンドが実行されたかどうかについての情報
  - stat: 配列の1つ目の場合、世代の確認結果、それ以外の場合はコマンドの実行結果(current: 世代が一致、different: 世代が不一致、done: コマンドが実行された、ok: コマンドがスキップされた、error: コマンドが失敗した)
  - stdout: 標準出力
  - stderr: エラー出力
* `states` : queryの実行結果
* `test` : testの結果



## APIリファレンス

submarine.jsの詳細な書き方については[こちら](./API.md)
