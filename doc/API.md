# APIリファレンス

* [collect](./API.md#collect)
  - [gen/array](./API.md#genarray)
  - [gen/func](./API.md#genfunc)
  - [gen/bash](./API.md#genbash)
  - [gen/ssh](./API.md#genssh)
  - [fil/func](./API.md#filfunc)
  - [fil/ping](./API.md#filping)
  - [fil/ssh](./API.md#filssh)
* [copy](./API.md#copy)
* [provision](./API.md#provision)
* [query](./API.md#query)
* [test](./API.md#test)

定義ファイルと実行結果のサンプルは[こちら](./USAGE.md)

## collect

* 形式
```
[{
  type: <String>ジェネレータ or フィルタの種類,
  その他、ジェネレータおよびフィルタに必要なオプション
}, ...]
```
* 結果
```
{
  hosts: [<String>ホスト名, ...]
}
```

サーバリストの定義。ホスト名の一覧が生成される

### gen/array

配列ジェネレータ

```
{
  type: 'gen/array',
  array: [<String>ホスト名, ...]
}
```

ホスト名やIPアドレスの配列を指定する。指定した配列が、そのまま結果のリストとなる

### gen/func

関数ジェネレータ

```
{
  type: 'gen/func',
  func: ()=>{
    ...
    return [<String>ホスト名, ...];
  }
}
```

戻り値に配列をとる関数を指定する。引数はなし。関数の実行結果がそのまま結果のリストとなる

### gen/bash

bashジェネレータ

```
{
  type: 'gen/bash',
  cmd: <String>コマンド文字列
}
```

bashでコマンド実行した結果の標準出力を、スペースもしくは改行で分割した配列となる

### gen/ssh

sshジェネレータ

```
{
  type: 'gen/ssh',
  opts: [sshのオプション, ...],
  host: <String>ssh先のホスト名,
  cmd: <String>ssh先で実行するコマンド
}
```

ssh先で実行した結果の標準出力を、スペースもしくは改行で分割した配列となる


### fil/func

```
{
  type: 'fil/func',
  func: (hosts)=>{

    return [<String>ホスト名, ...];
  }
}
```

戻り値に配列をとる関数を指定する。引数には、これより前の処理で生成されたホスト名の配列が渡される

関数の実行結果がそのまま結果のリストとなる


### fil/ping

```
{
  type: 'fil/ping',
  opts: [<String>pingのオプション, ...]
}
```

これより前の処理で生成されたホスト名の一覧に対して、pingを打ち、応答のあったホストだけのリストを返す


### fil/ssh

```
{
  type: 'fil/ssh',
  opts: [<String>sshのオプション, ...],
  cmd: <String>ssh先で実行するコマンド,
}
```

これより前の処理で生成されたホスト名の一覧に対して、ssh接続し、そこでコマンドを実行する。コマンド実行が成功(リターンコードが0)したホストだけのリストを返す



# copy

* 形式
```
{
  opts: [sshのオプション, ...],
  files: [{
    src: <String>コピー元のファイルパス(./files/以下の相対パス),
    dst: <String>コピー先のファイルパス(絶対パス)
  }, ...]
}
```

* 結果
```
 [
  {
   "stat": <String>コピー成功したか(done: 成功, error: 失敗),
    src: <String>コピー元のファイルパス(./files/以下の相対パス),
    dst: <String>コピー先のファイルパス(絶対パス)
    "stdout": <String>コピー(rsync)コマンドの標準出力,
    "stderr": <String>コピー(rsync)コマンドのエラー出力
  }, ...
]
```

サーバにコピーするファイルの一覧を定義します。コピー元のファイルは、カレントディレクトリの./files/以下に置く必要があります

コピーはrsync + sshで行われます

# provision

* 形式
```
{
  gen: <String>世代,
  opts: [<String>sshのオプション, ...],
  cmds: [{
    name: <String>ロックファイルの名前,
    cmd: <String>実行するコマンド
  }, ...]
}
```

* 結果
```
[
  {
    "stat": <String>サーバが定義と同世代か(current: 同世代、different: 同世代でない, error: 確認失敗),
    "stdout": <String>サーバの世代(世代を確認するコマンドの実行結果),
    "stderr": <String>世代確認コマンドのエラー出力
  },sultsという名前で、ホスト名out": <String>実行済みかどうか確認コマンドの標準出力,
    "stderr": <String>実行済みかどうか確認コマンドのエラー出力
  }, ...
]
```

サーバの構築コマンドを定義します。コマンドの実行結果が配列として返されます

配列の先頭は、サーバの世代チェックの結果が入ることになっています

# query

* 形式
```
{
  opts: [<String>sshのオプション],
  query: {
    クエリ名: <String>コマンド,
    ...
  }
}
```

* 結果
```
{
  クエリ名: <String>コマンドの実行結果,
  クエリ名: {
    stat: 'error',
    stdout: <String>コマンド実行失敗した際の標準出力,
    stderr: <String>コマンド実行失敗した際のエラー出力,
  },
  ...
}
```

サーバが正しく構築されたか、テストするための値を取得します。クエリはクエリ名とコマンドのkey-valueペアで定義し、クエリ名と実行結果のkey-valueペアとして返ってきます

コマンドの実行に失敗した場合は、`stat: 'error'` を含むエラーオブジェクトが返されます。特定のコマンドが失敗することをテストしたい場合に、エラーオブジェクトが返されたことを確認することができます

# test

* 形式
```
{
  func: (host, all)=>{
    ...(テスト)
    return <Any>テスト結果(戻り値は何でもよい)
  }
}
```

* 結果
```
<Any>テスト関数の戻り値
```

queryで取得した値をテストします。テスト関数には2つの引数があり、第一引数はあるサーバに対するquery結果、第二引数は、全サーバのクエリ結果(ホスト名-クエリ結果のkey-valueペア)が渡されます

第一引数はサーバ単体のテスト、第二引数は全サーバのクエリ結果を比較したい場合に使います

戻り値は何でも良いですが、どのテスト項目が成功/失敗したかを確認するために、テスト項目名とテスト結果のkey-valueペアを返すことを推奨します