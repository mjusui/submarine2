


const abc=async ()=>{
  const a=await Promise.resolve('a');
  const b=await Promise.reject('b');
  const c=await Promise.resolve('c');

  return a + b + c;
};


abc().then(console.log)
  .catch(console.err)
