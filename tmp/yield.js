

const array=['a','b','c'];

const Gen=function*(){
  let ret=[];

  for(const v of array){
    const val = yield Promise
      .resolve(v);

    ret=[...ret, val];
  };

  return ret;
}


const gen=Gen();

let item=gen.next();
const itrt=()=>{
  if(item.done){
    console.log(item);
    return;
  }
  const prom=item.value;

  prom.then(v =>{
    item=gen.next(v);
    itrt();
  });
}; itrt();

/* async/await */

const result=(async ()=>{
  let ret=[];

  for(let i=0;i < array.length;i++){
    const val=await Promise.resolve(array[i]);

    ret=[...ret, val];
  };

  return ret;
})();


result.then(console.log)
