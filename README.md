# このソフトウェアについて

Submarine.jsというサーバの構成管理&テストツールです

開発者がAnsibleを使っていた経験から、構成管理ツールにこんな機能あったらいいのに、というものを実装しています

プロトタイプとして[こちら](https://gitlab.com/mjusui/submarine)のsubmarine無印がありますが、今後(2020/12以降)は、このsubmarine2のリポジトリをベースに開発していきます

# 特徴

* Node.js(ほぼJSON)で構成情報を記述していきます
  * 単純なYAMLよりも、プログラマティックに記述することができるので、入れ子を分割してネストを減らせます
* 冪等性ではなく、イミュータブルなShellScriptの概念を採用しています
  * 冪等性に配慮して、膨大な数のモジュールを学んだり、if文を書く必要はありません
* サーバの世代管理の機能があります
  * 複数世代のサーバが並行稼働するような環境でも、安全にコードを実行できます
* 構成管理とテストを、1つのファイルに記述できます
  * テストする値も、モジュールではなく、ShellScriptを書いて取得します


# 前提条件

* Node.js 14.x系が動作すること
  * nvm環境がおすすめ: [インストール手順](https://nodejs.org/en/download/package-manager/#nvm)
* 管理対象のサーバにパスワードなしでssh + sudoできること
* 管理対象のサーバにrsyncでファイルをコピーできること
* 管理対象のサーバがpingに応答すること

Ubuntu 20.04でテストしていますが、その他のディストリビューションでも動作するはずです


# インストール方法

## nvm + Node.jsのインストール

* [こちら](https://nodejs.org/en/download/package-manager/#nvm)の手順にしたがって、nvmをインストールします
* Node.js 14.x系をインストールします

```
nvm install 14
```

## Submarine.jsのインストール
* 作業ディレクトリを作成します

```
mkdir work
cd work
```

* タグを指定してインストールします

```
curl -LO https://gitlab.com/mjusui/submarine2/-/archive/[タグ]/submarine2-[タグ].tar.gz
tar xzf submarine2-[タグ].tar.gz
mv submarine2-[タグ] submarine2
```

* submarineコマンドにPATHを通しておきます

```
export PATH="/path/to/work/submarine2/sbin:$PATH"
```

# 使い方

使い方は[こちら](./doc/USAGE.md)

