'use strict';
const assert=require('assert').strict;

const Conn=require('Conn');


Conn.bash({
  cmd: 'echo submarine-bash {1..3}',
}, (err, stdout, stderr)=>{
  assert(err === null, err);
  assert(stdout === 'submarine-bash 1 2 3');
  assert(typeof stderr === 'string');
});


Conn.ping({
  opts: [],
  host: 'target',
}, (err, stdout, stderr)=>{
  assert(err === null, err);
  assert(typeof stdout === 'string', stdout);
  assert(0 < stdout.length, stdout);
  assert(stderr === '', stderr);
});


Conn.ssh({
  opts: [ '-l submarine', ],
  host: 'target',
  cmd: String.raw`
    echo submarine-ssh
  `,
}, (err, stdout, stderr)=>{
  assert(err === null, err);
  assert(stdout === 'submarine-ssh', stdout);
  assert(typeof stderr === 'string', stderr);
});


const destdir=Math.floor(
  Math.random() * 1000
);
Conn.rsync({
  opts: ['-l submarine'],
  host: 'target',
  src: `${process.cwd()}/files/opt/`,
  dst: `/opt/${destdir}`,
}, (err, stdout, stderr)=>{
  assert(err === null, err);
  assert(stdout === '', stdout);
  assert(stderr === '', stderr);

  Conn.ssh({
    opts: ['-l submarine'],
    host: 'target',
    cmd: String.raw`
      cat /opt/${destdir}/b.txt \
        && sudo rm -rf /opt/${destdir}
    `,
  }, (err, stdout, stderr)=>{
    assert(err === null, err);
    assert(stdout === 'b', stdout);
    assert(stderr === '', stderr);
  });
});
