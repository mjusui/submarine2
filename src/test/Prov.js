'use strict';
const assert=require('assert').strict;
const Conn=require('Conn');
const Prov=require('Prov');


const gen=()=>{
  Prov.gen({
    store: '/var/lib/submarine',
    gen: 'provision-1',

    opts: ['-l submarine'],
    host: 'target',
  }, (err, stat, stdout, stderr)=>{
    assert(err === null, err);
    assert(stat === 'current', stat);
    assert(stdout === 'provision-1', stdout);
    assert(stderr === '', stderr);

    gen2();
  });
};

const gen2=()=>{
  Prov.gen({
    store: '/var/lib/submarine',
    gen: 'provision-2',

    opts: ['-l submarine'],
    host: 'target',
  }, (err, stat, stdout, stderr)=>{
    assert(err === null, err);
    assert(stat === 'different', stat);
    assert(stdout === 'provision-1', stdout);
    assert(stderr === '', stderr);

    run();
  });
};



const run=()=>{
  Prov.run({
    name: 'provision',
    store: '/var/lib/submarine',

    opts: ['-l submarine'],
    host: 'target',
    cmd: 'echo run provision',
  }, (err, stat, stdout, stderr)=>{
    assert(err === null, err);
    assert(stat === 'done', stat);
    assert(stdout === 'run provision', stdout);
    assert(stderr === '', stderr);

    run2();
  });
};

const run2=()=>{
  Prov.run({
    name: 'provision',
    store: '/var/lib/submarine',

    opts: ['-l submarine'],
    host: 'target',
    cmd: 'echo run provision',
  }, (err, stat, stdout, stderr)=>{
    assert(err === null, err);
    assert(stat === 'ok', stat);
    assert(stdout === 'ok', stdout);
    assert(stderr === '', stderr);

    postrun();
  });

};

const postrun=()=>{
  Conn.ssh({
    opts: ['-l submarine'],
    host: 'target',
    cmd: `sudo rm -rf \
      /var/lib/submarine`,
  }, (err, stdout, stderr)=>{
    if(err){
      console.error('rm', err, stdout, stderr);
      return;
    }
  })
};


gen();
