'use strict';
const assert=require('assert').strict;

const collect=[{
  type: 'gen/bash',
  cmd: 'echo {a..c} target',
}, {
  type: 'fil/ping',
}];


const copy={
  opts: ['-l submarine'],
  files: [{
    src: './opt/submarine/a.txt',
    dst: '/opt/submarine/a.txt',
  }, {
    src: './opt/submarine/b',
    dst: '/opt/submarine/',
  }],
};

const provision={
  gen: 'submarine-test-1',
  opts: ['-l submarine'],
  cmds: [{
    name: 'a',
    cmd: 'echo a',
  }, {
    name: 'b',
    cmd: 'echo b',
  }]
};


const query={
  opts: ['-l submarine'],
  query: {
    hostname: 'hostname -s',
    uname: 'uname -a',
    xxx: 'echo xxx',
    error: 'ehco error',

    a: String.raw`
      cat /opt/submarine/a.txt \
      && sudo rm -f /opt/submarine/a.txt
    `,
    b: String.raw`
      cat /opt/submarine/b/b.txt \
      && sudo rm -rf /opt/submarine/b
    `,
  },
}

const test={
  func: (host, all)=>{
    return {
      host_count: Object.keys(all).length === 1,
      hostname: host.hostname === 'ubu2004-submarine-target',
      xxx: host.xxx === 'xxx',
      error: typeof host.error === 'object'
        && host.error.stat === 'error',

      a: host.a === 'a',
      b: host.b === 'b',
    };
  },
};


module.exports={
  collect: collect,
  copy: copy,
  provision: provision,
  query: query,
  test: test,
};
