'use strict';
const assert=require('assert').strict;

const Coll=require('Coll');

Coll['gen/array'](['gen/array'], {
  array: ['b', 'c'],
}, (err, hosts)=>{
  assert(err === null, err);  
  assert(hosts[0] === 'gen/array', hosts);
  assert(hosts[1] === 'b', hosts);
  assert(hosts[2] === 'c', hosts);
  assert(hosts.length === 3, hosts);
});

Coll['gen/func'](['a'], {
  func: () => ['gen/func', 'c'],
}, (err, hosts)=>{
  assert(err === null, err);  
  assert(hosts[0] === 'a', hosts);
  assert(hosts[1] === 'gen/func', hosts);
  assert(hosts[2] === 'c', hosts);
  assert(hosts.length === 3, hosts);
});

Coll['gen/bash'](['a'], {
  cmd: 'echo {b..c} gen/bash',
}, (err, hosts)=>{
  assert(err === null, err);  
  assert(hosts[0] === 'a', hosts);
  assert(hosts[1] === 'b', hosts);
  assert(hosts[2] === 'c', hosts);
  assert(hosts[3] === 'gen/bash', hosts);
  assert(hosts.length === 4, hosts);
});

Coll['gen/ssh']([], {
  opts: ['-l submarine'],
  host: 'target',
  cmd: 'echo {a..c} gen/ssh',
}, (err, hosts)=>{
  assert(err === null, err);  
  assert(hosts[0] === 'a', hosts);
  assert(hosts[1] === 'b', hosts);
  assert(hosts[2] === 'c', hosts);
  assert(hosts[3] === 'gen/ssh', hosts);
  assert(hosts.length === 4,hosts);
});



Coll['fil/func'](['a','b'], {
  func: hosts => [hosts[0]]
}, (err, hosts)=>{
  assert(err === null, err);
  assert(hosts[0] === 'a', hosts);
  assert(hosts.length === 1, hosts);
});

Coll['fil/ping'](['target','x', 'target'], {
  opts: [],
}, (err, hosts)=>{
  assert(err === null, err);
  assert(hosts[0] === 'target', hosts);
  assert(hosts[1] === 'target', hosts);
  assert(hosts.length === 2, hosts);
});

Coll['fil/ssh'](['target','x', 'target'], {
  opts: ['-l submarine'],
  cmd: 'true',
}, (err, hosts)=>{
  assert(err === null, err);
  assert(hosts[0] === 'target', hosts);
  assert(hosts[1] === 'target', hosts);
  assert(hosts.length === 2, hosts);
});
