'use strict';
const cpus=require('os')
  .cpus()
  .length;

let limit=process
  .env
  .SUBMARINE_LIMIT;

limit=typeof limit === 'number'
  ? limit
  : cpus*4 ;


const Cap=class Cap {
  constructor(opt){
    this.limit=typeof opt.limit === 'number'
      ? opt.limit
      : limit ;
    this.count=0;
  }
  run(hndl){
    const poll=()=>{
      if(this.count < this.limit){
        this.count+=1;

        hndl(()=>{ this.count-=1; });
        return;
      }
      setTimeout(poll, 170);
    };
    setTimeout(poll, 170);
  }
}

const cap=new Cap({});

module.exports=cap;

