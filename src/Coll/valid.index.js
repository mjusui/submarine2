'use strict';
const assert=require('assert').strict;

const valid={};

valid['gen/array']=(opt)=>{
  const vopt={
    type: opt.type,
    array: opt.array,
  };
  assert(Array
    .isArray(vopt.array), vopt);
  return vopt;
};
valid['gen/func']=(opt)=>{
  const vopt={
    type: opt.type,
    func: opt.func,
  };
  assert(typeof vopt.func
    === 'function', vopt);
  return vopt;
};
valid['gen/bash']=(opt)=>{
  const vopt={
    type: opt.type,
    cmd: opt.cmd,
  };
  assert(typeof vopt.cmd
    === 'string', vopt);
  assert(0 < vopt.cmd.length, vopt);
  return vopt;
};
valid['gen/ssh']=(opt)=>{
  const vopt={
    type: opt.type,
    opts: opt.opts || [],
    host: opt.host,
    cmd: opt.cmd,
  };
  assert(Array
    .isArray(vopt.opts), vopt);
  assert(vopt.opts.every(
    o => typeof o === 'string'
  ), vopt);
  assert(typeof vopt.host
    === 'string', vopt);
  assert(0 < vopt.host.length, vopt);
  assert(typeof vopt.cmd
    === 'string', vopt);
  assert(0 < vopt.cmd.length, vopt);
  return vopt;
};
valid['fil/func']=valid['gen/func'];
valid['fil/ping']=(opt)=>{
  const vopt={
    type: opt.type,
    opts: opt.opts || [],
  };
  assert(Array
    .isArray(vopt.opts), vopt);
  assert(vopt.opts.every(
    o => typeof o === 'string'
  ), vopt);
  return vopt;
};
valid['fil/ssh']=(opt)=>{
  const vopt={
    type: opt.type,
    opts: opt.opts || [],
    cmd: opt.cmd || 'true',
  };
  assert(Array
    .isArray(vopt.opts), vopt);
  assert(vopt.opts.every(
    o => typeof o === 'string'
  ), vopt);
  assert(typeof vopt.cmd
    === 'string', vopt);
  assert(0 < vopt.cmd.length, vopt);
  return vopt;
};


module.exports={
  'gen/array': require('Coll/gen/array'),
  'gen/func': require('Coll/gen/func'),
  'gen/bash': require('Coll/gen/bash'),
  'gen/ssh': require('Coll/gen/ssh'),

  'fil/func': require('Coll/fil/func'),
  'fil/ping': require('Coll/fil/ping'),
  'fil/ssh': require('Coll/fil/ssh'),

  valid: valid,
};
