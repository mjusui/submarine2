'use strict';
const Conn=require('Conn');

module.exports=(hosts, opt, hndl)=>{
  const ret={
    hosts: [],
  };

  const proms=hosts.map(
  host => new Promise((resl, rejc)=>{
    const popt=Object.assign({}, opt, {
      host: host,
    });
    Conn.ping(popt, (err, stdout, stderr)=>{
      if(err){
        rejc(err);
        return;
      }
      resl(host);
    });
  }) );

  Promise.allSettled(proms).then(
    results => results.filter(
      result => result.status
         === 'fulfilled'
    ).map(result => result.value)
  ).then(hosts => hndl(null, hosts));
};
