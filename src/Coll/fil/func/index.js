'use strict';

module.exports=(hosts, opt, hndl)=>{
  try {
    const ret={
      hosts: opt.func(hosts)
    };

    hndl(null, ret.hosts);
  }catch(err){
    hndl(err, hosts);
  }
};
