'use strict';
const assert=require('assert').strict;

module.exports={
  'gen/array': require('Coll/gen/array'),
  'gen/func': require('Coll/gen/func'),
  'gen/bash': require('Coll/gen/bash'),
  'gen/ssh': require('Coll/gen/ssh'),

  'fil/func': require('Coll/fil/func'),
  'fil/ping': require('Coll/fil/ping'),
  'fil/ssh': require('Coll/fil/ssh'),
};
