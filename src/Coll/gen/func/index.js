'use strict';

module.exports=(hosts, opt, hndl)=>{
  try {
    const ret={
      hosts: opt.func(),
    };

    hndl(null, [...hosts,
      ...ret.hosts ]);
  }catch(err){
    hndl(err, hosts);
  }
};
