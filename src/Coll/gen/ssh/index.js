'use strict';
const Conn=require('Conn');

module.exports=(hosts, opt, hndl)=>{
  Conn.ssh(opt, (err, stdout, stderr)=>{
    if(err){
      hndl(err, hosts);
      return;
    }

    const ret={
      hosts: stdout
        .split(/\r\n|\r|\n| /)
        .filter(r => r),
    };

    hndl(err, [...hosts,
      ...ret.hosts]);
  });
};
