'use strict';
const assert=require('assert').strict;


module.exports={
  bash: require('Conn/bash'),
  ssh: require('Conn/ssh'),
  ping: require('Conn/ping'),

  rsync: require('Conn/rsync'),
};
