'use strict';
const { exec }=require('child_process');
const Cap=require('Cap');


module.exports=(opt, hndl)=>{
  const opts=([ 
    '-c 1',
    '-W 1',
    ...opt.opts,
    opt.host,
  ]).join(' ');

  const cmd=`ping ${opts}`;

  Cap.run(free =>{
    exec(cmd, (err, stdout, stderr)=>{
      free();

      const ret={
        stdout: stdout
          .replace(/(\r\n|\r|\n)$/, ''),
        stderr: stderr
          .replace(/(\r\n|\r|\n)$/, ''),
      };
 

      hndl(err,
        ret.stdout,
        ret.stderr );
    });
  });
};
