'use strict';
const { exec }=require('child_process');
const Cap=require('Cap');


module.exports=(opt, hndl)=>{
  const opts=([ 
    '-o StrictHostKeyChecking=no',
    '-o ConnectTimeout=6',
    ...opt.opts,
    opt.host,
  ]).join(' ');

  const enc=Buffer
    .from(opt.cmd)
    .toString('base64');

  const cmd=`echo ${enc}|ssh ${opts} "$(base64 -d)"`;

  Cap.run(free =>{
    exec(cmd, (err, stdout, stderr)=>{
      free();

      const ret={
        stdout: stdout
          .replace(/(\r\n|\r|\n)$/, ''),
        stderr: stderr
          .replace(/(\r\n|\r|\n)$/, ''),
      };
 

      hndl(err,
        ret.stdout,
        ret.stderr );
    });
  });
};
