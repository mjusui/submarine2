'use strict';
const { exec }=require('child_process');
const Cap=require('Cap');


module.exports=(opt, hndl)=>{
  const opts=([ 
    '-o StrictHostKeyChecking=no',
    '-o ConnectTimeout=6',
    ...opt.opts,
  ]).join(' ');


  const cmd=`rsync -az -e 'ssh ${opts}'`
    + ` --rsync-path='sudo mkdir -p $(dirname ${opt.dst}) && sudo rsync'`
    + ` ${opt.src} ${opt.host}:${opt.dst}`;

  Cap.run(free =>{
    exec(cmd, (err, stdout, stderr)=>{
      free();

      const ret={
        stdout: stdout
          .replace(/(\r\n|\r|\n)$/, ''),
        stderr: stderr
          .replace(/(\r\n|\r|\n)$/, ''),
      };
 

      hndl(err,
        ret.stdout,
        ret.stderr );
    });
  });
};
