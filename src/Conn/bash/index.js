'use strict';
const { exec }=require('child_process');
const Cap=require('Cap');


module.exports=(opt, hndl)=>{
  const enc=Buffer
    .from(opt.cmd)
    .toString('base64');

  const cmd=`echo ${enc}|bash -c "$(base64 -d)"`;

  Cap.run(free =>{
    exec(cmd, (err, stdout, stderr)=>{
      free();

      const ret={
        stdout: stdout
          .replace(/(\r\n|\r|\n)$/, ''),
        stderr: stderr
          .replace(/(\r\n|\r|\n)$/, ''),
      };
 

      hndl(err,
        ret.stdout,
        ret.stderr );
    });
  });
};
