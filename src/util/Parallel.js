'use strict';

const Parallel=class Parallel {
  constructor(){
    this.paras=[];

    Object.defineProperty(this, 'with', {
      set: (para)=>{
        this.paras=[
          ...this.paras,
          para,
        ];
      },
      get: ()=>{
        return this.paras[
          this.paras.length - 1
        ];
      }
    });
  }
  start(end, ...args){
    let cnt=0;
    const len=this.paras.length;
    const results=[];

    this.paras.forEach((para,idx)=>{
      let done=false;
      const resolve=(val)=>{
        if(done){
          return;
        }
        done=true;

        results[idx]=val;

        cnt+=1;
        if(cnt < len){
          return;
        }

        end(...results);
      };

      para(resolve, ...args);
    });
  }
}

module.exports=Parallel;

