'use strict';




const Step=class Step {
  constructor(){
    this.steps=[];

    Object.defineProperty(this, 'next', {
      set: (step)=>{
        this.steps=[
          ...this.steps,
          step,
        ];
      },
      get: ()=>{
        return this.steps[
          this.steps.length - 1
        ];
      },
    });
  }

  start(...args){
    let idx=0;

    const step=(...args)=>{
      const curr=this.steps[idx];
      const next=this.steps[idx+1]
        ? (...nextargs)=>{
            idx+=1;
            step(...nextargs);
          }
        : null ;

      curr(next, ...args);
    }; step(...args);
  }
}

try {
  module.exports=Step;
}catch(err){
  console.log('This is a client-side');
};
