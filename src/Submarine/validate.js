'use strict';



const validate=(val, ...msg)=>{
  if(val){
    return;
  }
  console.error(...msg);
  process.exit(1);
};

const UndefinedMsg=' is required: ';
validate.isRequired=(val, sub, ...msgs)=>{
  validate(!(val === undefined),
    sub + UndefinedMsg, ...msgs);
};

const InvalidArrayMsg=' must be an array: ';
validate.isArray=(val, sub, ...msgs)=>{
  validate(Array.isArray(val),
    sub + InvalidArrayMsg, ...msgs);
};

const InvalidStringMsg=' must be a string: ';
validate.isString=(val, sub, ...msgs)=>{
  validate(typeof val === 'string',
    sub + InvalidStringMsg, ...msgs);
};

const EmptyStringMsg=' must have some length: ';
validate.hasLength=(val, sub, ...msgs)=>{
  validate(0 < val.length,
    sub + EmptyStringMsg, ...msgs);
}

const InvalidFunctionMsg=' must be a function: ';
validate.isFunction=(val, sub, ...msgs)=>{
  validate(typeof val === 'function',
    sub + InvalidFunctionMsg, ...msgs);
};

const InvalidObjectMsg=' must be an object: ';
validate.isObject=(val, sub, ...msgs)=>{
  validate(typeof val === 'object',
    sub + InvalidObjectMsg, ...msgs);
};


module.exports=validate;
  
