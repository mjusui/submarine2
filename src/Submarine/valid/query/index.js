'use strict';
const validate=require('Submarine/validate');

const paths=require('paths');
const {
  query,
}=require(paths.submarine_js);



let vopt=null;

if(query){
  validate.isObject(query,
    '"query" object', query);
  vopt={
    opts: query.opts || [],
    query: query.query,
  };
  
  validate.isArray(vopt.opts,
    '"opts"', query);

  vopt.opts.forEach(opt =>{
    validate.isString(opt,
      'Item of "opts"', query);
  });
  
  validate.isObject(query.query,
    '"query"', query);

  const values=Object
    .values(vopt.query);

  values.forEach(cmd =>{
    validate.isString(cmd,
      'value of "query"', query);
    validate.hasLength(cmd,
      'value of "query"', query);
  });
}



module.exports=vopt;
