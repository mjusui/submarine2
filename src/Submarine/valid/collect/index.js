'use strict';
const validate=require('Submarine/validate');
const paths=require('paths');
const {
  collect,
}=require(paths.submarine_js);


let opts=null;

if(collect){
  const valid={};

  valid['gen/array']=(opt)=>{
    const vopt={
      type: opt.type,
      array: opt.array,
    };
    validate.isRequired(vopt.array,
      '"array"', opt);
    validate.isArray(vopt.array,
      '"array"', opt);

    vopt.array.forEach(host =>{
      validate.isString(host,
        'Item of "array"', opt);
      validate.hasLength(host,
        'Item of "array"', opt);
    });
    return vopt;
  };
  valid['gen/func']=(opt)=>{
    const vopt={
      type: opt.type,
      func: opt.func,
    };
    validate.isRequired(vopt.func,
      '"func"', opt);
    validate.isFunction(vopt.func,
      '"func"', opt);
    return vopt;
  };
  valid['gen/bash']=(opt)=>{
    const vopt={
      type: opt.type,
      cmd: opt.cmd,
    };
    validate.isRequired(vopt.cmd,
      '"cmd"', opt);
    validate.isString(vopt.cmd,
      '"cmd"', opt);
    validate.hasLength(vopt.cmd,
      '"cmd"', opt);
    return vopt;
  };
  valid['gen/ssh']=(opt)=>{
    const vopt={
      type: opt.type,
      opts: opt.opts || [],
      host: opt.host,
      cmd: opt.cmd,
    };
    validate.isArray(vopt.opts,
      '"opts"', opt);

    vopt.opts.forEach(lopt =>{
      validate.isString(lopt,
        'Item of "opts"', opt);
    });
  
    validate.isRequired(vopt.host,
      '"host"', opt);
    validate.isString(vopt.host,
      '"host"', opt);
    validate.hasLength(vopt.host,
      '"host"', opt);
  
    validate.isRequired(vopt.cmd,
      '"cmd"', opt);
    validate.isString(vopt.cmd,
      '"cmd"', opt);
    validate.hasLength(vopt.cmd,
      '"cmd"', opt);
  
    return vopt;
  };
  valid['fil/func']=valid['gen/func'];
  valid['fil/ping']=(opt)=>{
    const vopt={
      type: opt.type,
      opts: opt.opts || [],
    }; 
    validate.isArray(vopt.opts,
      '"opts"', opt);

    vopt.opts.forEach(lopt =>{
      validate.isString(lopt,
        'Item of "opts"', opt);
    });
    return vopt;
  };
  valid['fil/ssh']=(opt)=>{
    const vopt={
      type: opt.type,
      opts: opt.opts || [],
      cmd: opt.cmd,
    };
    validate.isArray(vopt.opts,
      '"opts"', opt);

    vopt.opts.forEach(lopt =>{
      validate.isString(lopt,
        '"opts"', opt);
    });
  
    validate.isRequired(vopt.cmd,
     '"cmd"', opt);
    validate.isString(vopt.cmd,
      '"cmd"', opt);
    validate.hasLength(vopt.cmd,
      '"cmd"', opt);
  
    return vopt;
  
  };
  
  const route=(opt)=>{
    validate.isRequired(opt.type,
     '"type"', opt);
    validate(Object.keys(valid)
      .includes(opt.type),
    '"type" is invalid: ', opt);
  
    return valid[opt.type](opt);
  };

  validate.isArray(collect,
    '"collect"', collect);

  collect.forEach(lopt =>{
    validate.isObject(lopt,
      'Item of "collect"', collect);
  });

  opts=collect.map(
    opt => route(opt)
  );
}

module.exports=opts;
