'use strict';
const validate=require('Submarine/validate');

const paths=require('paths');
const {
  provision,
}=require(paths.submarine_js);


let vopt=null;

if(provision){
  validate.isObject(provision,
    '"provision" object', provision);

  vopt={
    gen: provision.gen,
    store: provision.store
      || '/var/lib/submarine',

    opts: provision.opts || [],
    cmds: provision.cmds || [],
  };

  validate.isRequired(vopt.gen,
    '"gen"', provision);
  validate.isString(vopt.gen,
    '"gen"', provision);
  validate.hasLength(vopt.gen,
    '"gen"', provision);
  validate(vopt.gen.match(
    /^[a-zA-Z0-9-]+$/
  ), '"gen" must consist of [a-zA-Z0-9-]',
    provision);
  
  validate.isString(vopt.store,
    '"store"', provision);
  validate.hasLength(vopt.store,
    '"store"', provision);
  
  validate.isArray(vopt.opts,
    '"opts"', provision);

  vopt.opts.forEach(opt =>{
    validate.isString(opt,
      'Item of "opts"', provision);
  });
  
  validate.isArray(vopt.cmds,
    '"cmds"', provision);

  vopt.cmds=vopt.cmds.map(cmd =>{
    validate.isObject(cmd,
      'Item of "cmds"', provision);

    const vcmd={
      name: cmd.name,
      cmd: cmd.cmd,  
    };

    validate.isRequired(vcmd.name,
      '"name" atribute of "cmds" item', provision);
    validate.isString(vcmd.name,
      '"name" attribute of "cmds" item', provision);
    validate.hasLength(vcmd.name,
      '"name" attribute of "cmds" item', provision);
    validate(vcmd.name.match(
      /^[a-zA-Z0-9]+$/
    ), '"name" attribute of "cmds" item must consist of [a-zA-Z0-9-]',
      provision); 

    validate.isRequired(vcmd.cmd,
      '"cmd" atriburte of "cmds" item', provision);
    validate.isString(vcmd.cmd,
      '"cmd"', provision);
    validate.hasLength(vcmd.cmd,
      '"cmd"', provision);

    return vcmd;
  });
}


module.exports=vopt;
