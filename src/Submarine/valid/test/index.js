'use strict';
const validate=require('Submarine/validate');

const paths=require('paths');
const {
  test
}=require(paths.submarine_js);


let vopt=null;

if(test){
  validate.isObject(test,
    '"test" object', test);

  vopt={
    func: test.func,
  };
  
  validate.isRequired(vopt.func,
    '"func"', test);
  validate.isFunction(vopt.func,
    '"func"', test);
}



module.exports=vopt;
