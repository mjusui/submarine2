'use strict';
const validate=require('Submarine/validate');

const paths=require('paths');
const {
  copy,
}=require(paths.submarine_js);


let vopt=null;

if(copy){
  validate.isObject(copy,
    '"copy" object', copy);

  vopt={
    opts: copy.opts || [],
    files: copy.files || [],
  };
  validate.isArray(vopt.opts,
    '"opts"', copy);

  vopt.opts.forEach(opt =>{
    validate.isString(opt,
      'Item of "opts"', copy);
  });

  validate.isArray(vopt.files,
    '"files"', copy);

  vopt.files=vopt.files.map(file =>{
    validate.isObject(file,
      'Item of "files"', copy);

    validate.isRequired(file.src,
      '"src" of "files" item', copy);
    validate.isString(file.src,
      '"src"', copy);
    validate.hasLength(file.src,
      '"src"', copy);

    validate.isRequired(file.dst,
      '"dst" of "files" item', copy);
    validate.isString(file.dst,
      '"dst"', copy);
    validate.hasLength(file.dst,
      '"dst"', copy);

    const vfile={
      src: `${paths.files}/${file.src}`,
      dst: file.dst,
    };

    return vfile;
  });

}

module.exports=vopt;
