'use strict';



module.exports={
  collect: require('Submarine/valid/collect'),
  copy: require('Submarine/valid/copy'),
  provision: require('Submarine/valid/provision'),
  query: require('Submarine/valid/query'),
  test: require('Submarine/valid/test'),
};
