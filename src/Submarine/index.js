'use strict';
const Step=require('util/Step');
const Parallel=require('util/Parallel');

const Conn=require('Conn');
const Coll=require('Coll');
const Prov=require('Prov');


const run=(hndl)=>{
  const {
    collect,
    copy,
    provision,
    query,
    test,
  }=require('Submarine/valid');

  const step=new Step();
  
  step.next=(next)=>{
    if(!collect){
      console.error('"collect" must be declared');
      return;
    }

    const lstep=new Step();
  
    collect.forEach(opt =>{
      const coll=Coll[opt.type];
  
      lstep.next=(next, hosts)=>{
        coll(hosts, opt, (err, hosts)=>{
          if(err){
            hndl({
              stat: 'error',
              msg: `Failed to collect hosts: ${JSON.stringify(opt)}`,
              hosts: hosts,
            });
            return;
          }
          next(hosts);
        });
      };
    });
    lstep.next=(none, hosts)=>{
      next(hosts); 
    };
    lstep.start([]);
  };
  


  step.next=(next, hosts)=>{
    if(!copy){
      next(hosts, null);
      return;
    }

    const opts=copy.files.map(
      file => ({
        opts: copy.opts,
        src: file.src,
        dst: file.dst,
      })
    );

    const lpara=new Parallel();

    const serial=(done, host)=>{
      const llstep=new Step();

      opts.forEach(opt =>{
        const lopt=Object.assign({}, opt, {
          host: host,
        });
        llstep.next=(next, results)=>{
          Conn.rsync(lopt, (err, stdout, stderr)=>{
            const result={
              src: opt.src,
              dst: opt.dst,
              stdout: stdout,
              stderr: stderr,
            };
            if(err){
              result.stat='error';
              results=[...results, result];
              done([host, results, err]);
              return;
            }
            result.stat='done';
            results=[...results, result];
            next(results);
          });
        };
      });
      llstep.next=(none, results)=>{
        done([host, results, null]);
      };
      llstep.start([]); 
    };

    hosts.forEach(host =>{
      lpara.with=(done)=>{
        serial(done, host);
      };
    });

    lpara.start((...fils)=>{
      const err=fils.find(
        fil => fil[2]
      );
      const files=Object
        .fromEntries(fils);
      if(err){
        hndl({
          stat: 'error',
          msg: `Failed to copy files`,
          hosts: hosts,
          files: files,
        });
        return;
      }
      next(hosts, files);
    });
  };
  


  step.next=(next, hosts, files)=>{
    if(!provision){
      next(hosts, files, null);
      return;
    }

    const opts=provision.cmds.map(
      cmd => ({
        name: cmd.name,
        store: provision.store,
  
        opts: provision.opts,
        cmd: cmd.cmd,
      })
    );
  
  
    const lpara=new Parallel();
  
    const serial=(done, host)=>{
      const lstep=new Step();

      lstep.next=(next, results)=>{
        const genopt={
          gen: provision.gen,
          store: provision.store,

          opts: provision.opts,
          host: host,
        };
        Prov.gen(genopt, (err ,stat, stdout, stderr)=>{
          results=[...results, {
            stat: stat,
            stdout: stdout,
            stderr: stderr,
          }];
          if(err){
            done([host, results, err])
            return;
          }
          if( !(stat === 'current') ){
            done([host, results, null]);
            return;
          }
          next(results);
        });
      };
  
      opts.forEach(opt =>{
        const lopt=Object.assign({}, opt, {
          host: host,
        });
        lstep.next=(next, results)=>{
          Prov.run(lopt, (err, stat, stdout, stderr)=>{
            results=[...results, {
              stat: stat,
              stdout: stdout,
              stderr: stderr,
            }];
            if(err){
              done([host, results, err]);
              return;
            }
            next(results);
          });
        };
      });
      lstep.next=(none, results)=>{
        done([host, results, null]);
      };
      lstep.start([]);
    };
  
    hosts.forEach(host =>{
      lpara.with=(done)=>{
        serial(done, host);
      };
    });
  
    lpara.start((...progs)=>{
      const err=progs.find(
        prog => prog[2]
      );
      const results=Object
        .fromEntries(progs);
  
      if(err){
        hndl({
          stat: 'error',
          msg: 'Failed to provision',
          hosts: hosts,
          files: files,
          results: results,
        });
        return;
      }
      next(hosts, files, results);
    });
  };
  
  step.next=(next, hosts, files, results)=>{
    if(!query){
      next(hosts, files, results, null);
      return;
    }


    const entries=Object
      .entries(query.query);
  
    const lpara=new Parallel();
  
    const parallel=(done, host)=>{
      const llpara=new Parallel();
  
      entries.forEach(ents=>{
        const key=ents[0];
        const cmd=ents[1];
  
        const popt={
          opts: query.opts,
          host: host,
          cmd: cmd,
        };
  
        llpara.with=(done)=>{
          Conn.ssh(popt, (err, stdout, stderr)=>{
            if(err){
              done([key,{ stat: 'error',
                stdout, stderr}])
              return;
            }
            done([key, stdout]);
          });
        };
      });
  
      llpara.start((...stats)=>{
        const states=Object
          .fromEntries(stats);

        done([host, states]);
      });
    };
  
    hosts.forEach(host =>{
      lpara.with=(done)=>{
        parallel(done, host);
      };
    });
  
    lpara.start((...stats)=>{
      const states=Object
        .fromEntries(stats);

      next(hosts, files, results, states);
    });
  };
  
  step.next=(next, hosts, files, results, states)=>{
    if(!test){
      next(hosts, files, results, states, null);
      return;
    }

    const entries=Object
      .entries(states);
  
    let scores=entries.map(ents =>{
      const host=ents[0];
      const state=ents[1];
  
      return [host, test.func(state, states)];
    });  
  
    scores=Object
      .fromEntries(scores);
  
    next(hosts, files, results, states, scores);
  };
  
  step.next=(none, hosts, files, results, states, scores)=>{
    hndl({
      stat: 'complete',
      msg: null,
      hosts: hosts,
      files: files,
      results: results,
      states: states,
      scores: scores,
    });
  };

  step.start()
};


module.exports={
  run: run,
};
