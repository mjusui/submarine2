'use strict';
const assert=require('assert').strict;
const Conn=require('Conn');

const gen=(opt, hndl)=>{
  const { gen, store, }=opt;

  const cmd=String.raw`
    set -ue -o pipefail

    gen=${gen}
    path=${store}/gen

    test -f $path || {
      sudo mkdir -p \
        -m 1777 \
        ${store} \
      && echo $gen \
        > $path
    } && cat $path
  `;

  const popt=Object.assign({}, opt, {
    cmd: cmd,
  });

  Conn.ssh(popt, (err ,stdout, stderr)=>{
    if(err){
      hndl(err, 'error', stdout, stderr);
      return;
    }
    if( !(stdout === gen) ){
      hndl(err, 'different', stdout, stderr);
      return;
    }
    hndl(err, 'current', stdout, stderr);
  });
};

const run=(opt, hndl)=>{
  const { store, name }=opt;

  const prerun=()=>{
    const cmd=String.raw`
      set -ue -o pipefail

      stat=none
      path=${store}/lock-${name}

      if [ -f $path ]
      then
        stat=$(cat $path)
      else
        sudo mkdir -p \
          -m 1777 \
        ${store} \
        && echo doing \
          > $path
      fi

      echo $stat
    `;

    const popt=Object.assign({}, opt, {
      cmd: cmd,
    });

    Conn.ssh(popt, (err, stdout, stderr)=>{
      if(err){
        hndl(err, 'error', stdout, stderr);
        return;
      }
      if( !(stdout === 'none') ){
        hndl(err, stdout, stdout, stderr);
        return;
      }
      run();
    });
  };

  const ret={};
  const run=()=>{
    Conn.ssh(opt, (err, stdout, stderr)=>{
      if(err){
        hndl(err, 'error',  stdout, stderr);
        return;
      }
      ret.err=err;
      ret.stdout=stdout;
      ret.stderr=stderr;

      postrun();
    });
  };

  const postrun=()=>{
    const cmd=String.raw`
      echo ok \
        > ${store}/lock-${name}
    `;

    const popt=Object.assign({}, opt, {
      cmd: cmd,
    });

    Conn.ssh(popt, (err, stdout, stderr)=>{
      if(err){
        hndl(err, 'error', stdout, stderr);
        return;
      }
      hndl(ret.err,
        'done',
        ret.stdout,
        ret.stderr);
    });

  };

  prerun();
};


module.exports={
  gen: gen,
  run: run,
};
